package com.jy.security.config.matchers;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

@Component("rbacServiceImpl")
public class RbacServiceImpl implements RbacService {

	private AntPathMatcher antPathMatcher = new AntPathMatcher();
	
	@Override
	public boolean hasPermission(HttpServletRequest request, Authentication authentication) {
		//拿到当前请求信息
		Object principal = authentication.getPrincipal();
		
		boolean hasPermission = false;
		//如果是UserDetail才判断，否则当前请求的用户就是没有经过身份认证的，直接返回false
		if(principal instanceof UserDetails){
			//获取当前用户的用户名
			String username = ((UserDetails) principal).getUsername();
			//之后就可以根据用户名去数据库中查询当前用户具有角色有权限访问的所有URL，下面我们只是模拟
			Set<String> urls = new HashSet<String>();
			urls.add("/a");
			for(String url : urls){
				//由于请求的URL可能是/user/123 而数据库中存放的可能是/user/* 这时候我们匹配需要一个工具类
				if(antPathMatcher.match(url, request.getRequestURI())){
					hasPermission = true;
					break;
				}
				
			}
			
		}
		
		return hasPermission;
	}

}
