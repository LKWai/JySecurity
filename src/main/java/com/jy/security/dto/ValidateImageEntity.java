package com.jy.security.dto;

import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.time.LocalDateTime;

//验证码对象
public class ValidateImageEntity implements Serializable {
	
	//验证码图片
	private BufferedImage image;
	
	//验证码字符
	private String code;
	
	//验证码过期时间
	private LocalDateTime expireTime;
	
	public ValidateImageEntity(String code, int expireIn) {
		this.code = code;
		this.expireTime = LocalDateTime.now().plusSeconds(expireIn);
	}
	
	//设置过期时间点
	public ValidateImageEntity(BufferedImage image, String code, int expireIn) {
		this.image = image;
		this.code = code;
		this.expireTime = LocalDateTime.now().plusSeconds(expireIn);
	}

	public ValidateImageEntity(BufferedImage image, String code, LocalDateTime expireTime) {
		this.image = image;
		this.code = code;
		this.expireTime = expireTime;
	}
	
	
	public boolean isExpried(){
		return LocalDateTime.now().isAfter(expireTime);
	}
	
	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public LocalDateTime getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(LocalDateTime expireTime) {
		this.expireTime = expireTime;
	}
	
}
