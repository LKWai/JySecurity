package com.jy.security.dao;

import java.util.Collection;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.jy.security.dto.MyGrantedAuthority;

import io.lettuce.core.dynamic.annotation.Param;

@Mapper
public interface LoginDao {
	
	@Select("select password from user where username = #{username}")
	public String selectPassWordByUserName(@Param("username") String username);
	@Select("select id from user where username = #{username}")
	public int selectIdByUserName(@Param("username") String username);
	@Select("select power from power where userid = #{id}")
	public Collection<MyGrantedAuthority> selectUserPower(@Param("id") int id);

}
