package com.jy.security.properties;

public class LoginProperties {

	private LoginPageProperties loginPageProperties = new LoginPageProperties();

	private LogOutPageProperties logOutPageProperties = new LogOutPageProperties();
	
	public LoginPageProperties getLoginPageProperties() {
		return loginPageProperties;
	}

	public void setLoginPageProperties(LoginPageProperties loginPageProperties) {
		this.loginPageProperties = loginPageProperties;
	}

	public LogOutPageProperties getLogOutPageProperties() {
		return logOutPageProperties;
	}

	public void setLogOutPageProperties(LogOutPageProperties logOutPageProperties) {
		this.logOutPageProperties = logOutPageProperties;
	}
	
}
