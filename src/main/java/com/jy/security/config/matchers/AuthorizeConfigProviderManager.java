package com.jy.security.config.matchers;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;

/*
 * 这个是接口的实现实际上就是对所有实现AuthorizeConfigProvider接口的类，将其都配置到security，让其配置生效
 */
public interface AuthorizeConfigProviderManager {
	
	void config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config);


}
