# JySecurity

#### 项目介绍
   在Java web开发领域中，开发一个鉴权与认证的模块一直是一个重要且十分复杂的任务，且由于开发人员水平不一，导致系统出现各种逻辑BUG或技术BUG，让黑产者有可乘之机，严重威胁了企业信息安全、财产安全等等。由此可见，开发一个松耦合、可配置、安全可靠的安全模块成了重中之重。由此本人开发出命名为JySecurity的安全模块。
   JySecurity基于Spring Boot和Spring Security开发，提供了完善的鉴权、认证、验证码、Session共享、Remember Me、防止CSRF攻击及其他可配置的十余项功能。本项目提供了可覆盖的配置选项、可实现的接口方法，最大限度的满足企业开发中安全的需求。无论是在分布式架构还是集群环境都可无缝适配。且使用方便，只需要在自己项目的Maven中依赖本模块即可使用。

#### 软件架构
基于Spring Boot 2.0、Spring Security开发完成


#### 安装教程、使用说明

请查看Word文档

#### 本人技术博客
更多Spring Security 微服务架构知识请访问
[https://my.oschina.net/u/3637243](https://my.oschina.net/u/3637243)


