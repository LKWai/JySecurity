package com.jy.security.properties;

public class LogOutPageProperties {
	
	private String logoutUrl = "/signOut";
	
	private String logoutSuccessUrl = "/BasicLoginPage.html";
	
	private String deleteCookies = "JSSIONID";

	public String getLogoutUrl() {
		return logoutUrl;
	}

	public void setLogoutUrl(String logoutUrl) {
		this.logoutUrl = logoutUrl;
	}

	public String getLogoutSuccessUrl() {
		return logoutSuccessUrl;
	}

	public void setLogoutSuccessUrl(String logoutSuccessUrl) {
		this.logoutSuccessUrl = logoutSuccessUrl;
	}

	public String getDeleteCookies() {
		return deleteCookies;
	}

	public void setDeleteCookies(String deleteCookies) {
		this.deleteCookies = deleteCookies;
	}
	
}
