package com.jy.security.securityconfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import com.jy.security.dao.LoginDao;
import com.jy.security.dto.UserDateilsExceptionEntity;

@Component
public class MyUserDetailsService implements UserDetailsService{
	
	@Autowired
	private LoginDao loginDao;
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		String password  = loginDao.selectPassWordByUserName(username);
		boolean hasDelete = true;
		userDateils(new UserDateilsExceptionEntity(password, hasDelete, hasDelete, hasDelete, hasDelete));
		return new User(username,password,loginDao.selectUserPower(loginDao.selectIdByUserName(username)));
	}
	
	private static void userDateils(UserDateilsExceptionEntity userDateilsExceptionEntity ){
		if(userDateilsExceptionEntity.getPassWord() == null){
			throw new UserDateilsException("用户不存在");
		}if(!userDateilsExceptionEntity.isAccountNonExpired()){
			throw new UserDateilsException("用户已过期");
		}if(!userDateilsExceptionEntity.isAccountNonLocked()){
			throw new UserDateilsException("用户被锁定，请联系管理员");
		}if(!userDateilsExceptionEntity.isEnabled()){
			throw new UserDateilsException("因违反用户条例，此用户已被永久删除");
		}if(!userDateilsExceptionEntity.isCredentialsNonExpired()){
			throw new UserDateilsException("密码过期，请更改密码");
		}
	}

	
}
