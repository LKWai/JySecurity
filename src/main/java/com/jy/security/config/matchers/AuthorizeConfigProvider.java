package com.jy.security.config.matchers;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;

/*
 * 因为现在所有的URL的权限配置都是写在SecurityConfig这个类中，但是当有人使用我们的安全模块的时候，我们并不知道使用者有哪些
 * URL需要什么样的权限，而使用者也不能直接在我们的配置类中去加代码，这时候我们需要提供一个接口让用户实现其URL的所需要的权限表达式
 * ，然后我们再写一个管理器去扫描所有实现了AuthorizeConfigProvider这个接口的实现类，将其配置生效到我们的安全模块
 */
public interface AuthorizeConfigProvider {
	
	//ExpressionInterceptUrlRegistry是authorizeRequests方法的返回类型，实际上security也是通过这个方法的返回对象来判断是否能访问
	void config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config);

}
