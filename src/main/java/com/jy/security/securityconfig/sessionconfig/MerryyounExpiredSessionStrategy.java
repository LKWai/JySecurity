package com.jy.security.securityconfig.sessionconfig;

import java.io.IOException;

import javax.servlet.ServletException;

import org.springframework.security.web.session.SessionInformationExpiredEvent;
import org.springframework.security.web.session.SessionInformationExpiredStrategy;

public class MerryyounExpiredSessionStrategy implements SessionInformationExpiredStrategy {

	@Override
	public void onExpiredSessionDetected(SessionInformationExpiredEvent event) throws IOException, ServletException {
		// TODO Auto-generated method stub
		//event.getResponse().setContentType("charset=UTF-8");
		event.getResponse().setHeader("content-type", "text/html;charset=UTF-8");
		event.getResponse().getWriter().print("<meta http-equiv=\"refresh\" content=\"1\">");//一秒钟自动刷新
		event.getResponse().getWriter().print("<script>alert('你的账号在另一个地点登录');</script>");
	}

}
