package com.jy.security.config.matchers;

import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
@Order(Integer.MAX_VALUE)//最后读取这个配置
public class RbacAuthorizeConfigProviderImpl implements AuthorizeConfigProvider {

	@Override
	public void config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
		// TODO Auto-generated method stub
		
		config.anyRequest().access("@rbacServiceImpl.hasPermission(request,authentication)")
		;
				
	}

}
