/**
 * 
 */
package com.jy.security.config.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.jy.security.properties.JySecurityProperties;

/**
 * @author root
 *
 */
@Component
public class MyAuthenticationFailureHandler implements AuthenticationFailureHandler {
	private Logger logger = LoggerFactory.getLogger(getClass());
	/* (non-Javadoc)
	 * @see org.springframework.security.web.authentication.AuthenticationFailureHandler#onAuthenticationFailure(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.security.core.AuthenticationException)
	 */
	@Autowired
	private JySecurityProperties jySecurityProperties;
	
	@Override
	public void onAuthenticationFailure(HttpServletRequest arg0, HttpServletResponse response, AuthenticationException authentication)
			throws IOException, ServletException {
		
		logger.info(authentication.getMessage()+"---登录失败");
		byte [] errors = authentication.getMessage().getBytes("UTF-8");
		String error = new String(errors,"iso-8859-1");
		response.setHeader("content-type","text/html;charset=UTF-8");
		response.sendRedirect(jySecurityProperties.getLoginProperties().getLoginPageProperties().getLoginPageURL()+"?error="+error);
		//response.getWriter().println("<script>alert('用户名或密码错误');</script>");
	}

}
