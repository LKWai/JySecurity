/**
 * 
 */
package com.jy.security.securityconfig.validatecodeconfig;

import org.springframework.security.core.AuthenticationException;

/**
 * @author Administrator
 *
 */
//AuthenticationException是security在身份认证过程中抛出异常的基类
public class ValidateImageException extends AuthenticationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8903062350747703361L;
	
	public ValidateImageException(String msg) {
		super(msg);
	}

}
