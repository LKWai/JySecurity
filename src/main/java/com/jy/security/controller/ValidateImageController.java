package com.jy.security.controller;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.web.HttpSessionSessionStrategy;
import org.springframework.social.connect.web.SessionStrategy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;

import com.jy.security.dto.ValidateImageEntity;
import com.jy.security.properties.JySecurityProperties;

@RestController
public class ValidateImageController {
	
	//Spring 工具类 用来操纵Session
	private SessionStrategy sessionStrategy = new HttpSessionSessionStrategy();
	
	public static String SESSION_KEY = "SESSION_KEY_IMAGE_CODE";
	@Autowired
	private JySecurityProperties jySecurityProperties;
	@RequestMapping("/validate/image")
	public void createValidateImage(HttpServletRequest request,HttpServletResponse response) throws IOException{
		//生成验证码
		ValidateImageEntity validateImageEntity = createValidateImage(request);
		//Session存到redis的时候，由于图片类是JDK本身的，不能实现序列化所以只将图片的数字和过期时间存到session中
		ValidateImageEntity validateImageEntityToRedis = new ValidateImageEntity(validateImageEntity.getCode(),jySecurityProperties.getLoginProperties().getLoginPageProperties().getValidateCodeImageTimeOut());
		//验证码放到session
		sessionStrategy.setAttribute(new ServletWebRequest(request), SESSION_KEY, validateImageEntityToRedis);
		//把验证码写回去 
		ImageIO.write(validateImageEntity.getImage(), "JPEG", response.getOutputStream());
	}
	
	
	//创建验证码的方法
	private ValidateImageEntity createValidateImage(HttpServletRequest request){
		
		int width = jySecurityProperties.getLoginProperties().getLoginPageProperties().getValidateCodeImageWidth();
		int height = jySecurityProperties.getLoginProperties().getLoginPageProperties().getValidateCodeImageHight();
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		
		Graphics g = image.getGraphics();
		
		Random random = new Random();
		
		g.setColor(getRandColor(200,250));
		g.fillRect(0, 0, width, height);
		g.setFont(new Font("Times New Roman", Font.ITALIC, 20));
		g.setColor(getRandColor(160,200));
		for(int i = 0;i < 155;i++){
			
			int x = random.nextInt(width);
			int y = random.nextInt(height);
			int xl = random.nextInt(12);
			int yl = random.nextInt(12);
			g.drawLine(x, y,x + xl,y + yl);
		}
		
		String sRand = "";
		for(int i = 0;i < jySecurityProperties.getLoginProperties().getLoginPageProperties().getValidateCodeLenth();i++){
			String rand = String.valueOf(random.nextInt(10));
			sRand += rand;
			g.setColor(new Color(20 + random.nextInt(110), 20 + random.nextInt(110), 20 + random.nextInt(110)));
			g.drawString(rand, 13*i+6,16);
		}
		
		
		g.dispose();
		
		return new ValidateImageEntity(image, sRand, jySecurityProperties.getLoginProperties().getLoginPageProperties().getValidateCodeImageTimeOut());
		
	}
	
	//生成随机背景条纹
	private Color getRandColor(int fc ,int bc){
		
		Random random = new Random();
		
		if(fc > 255){
			fc = 255;
		}
		if(bc > 255){
			bc = 255;
		}
		int r = fc +random.nextInt(bc -fc);
		int g = fc +random.nextInt(bc -fc);
		int b = fc +random.nextInt(bc -fc);
		return new Color(r,g,b);
		
	}

}
