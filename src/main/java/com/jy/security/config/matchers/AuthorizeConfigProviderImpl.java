package com.jy.security.config.matchers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

import com.jy.security.properties.JySecurityProperties;

//我们先写一个自己的实现，就是把SecurityConfig类中的配置拿过来
@Component
@Order(Integer.MIN_VALUE)//最先读取
public class AuthorizeConfigProviderImpl implements AuthorizeConfigProvider {
		
	@Autowired
	private JySecurityProperties jySecurityProperties;
	
	@Override
	public void config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
		// TODO Auto-generated method stub
		config.antMatchers(jySecurityProperties.getLoginProperties().getLoginPageProperties().getLoginPageURL()
				,jySecurityProperties.getLoginProperties().getLoginPageProperties().getLoginURL()
				,jySecurityProperties.getLoginProperties().getLogOutPageProperties().getLogoutUrl()
				,jySecurityProperties.getSessionManagementProperties().getSessionTimeOutUrl()
				,"/validate/image").permitAll();//不需要认证和权限的URL
				
	}

}
