package com.jy.security.config.matchers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;

public interface RbacService {
	
	boolean hasPermission(HttpServletRequest request,Authentication authentication);

}
