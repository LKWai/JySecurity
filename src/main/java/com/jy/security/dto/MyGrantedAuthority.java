package com.jy.security.dto;

import org.springframework.security.core.GrantedAuthority;

public class MyGrantedAuthority implements GrantedAuthority {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5528677442380024606L;

	private String power;
	
	public String getPower() {
		return power;
	}

	public void setPower(String power) {
		this.power = power;
	}

	@Override
	public String getAuthority() {
		// TODO Auto-generated method stub
		return power;
	}

}
