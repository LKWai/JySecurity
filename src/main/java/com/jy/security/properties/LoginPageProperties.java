package com.jy.security.properties;

public class LoginPageProperties {
	
	private String LoginPageURL = "/BasicLoginPage.html";
	
	private String LoginURL = "/ToLogin";
	
	private int ValidateCodeLenth = 4;
	
	private int ValidateCodeImageHight = 23;
	
	private int ValidateCodeImageWidth = 67;
	
	private int ValidateCodeImageTimeOut = 60;
	
	private int RememberMeTime = 60*60;
	
	public String getLoginPageURL() {
		return LoginPageURL;
	}

	public void setLoginPageURL(String loginPageURL) {
		LoginPageURL = loginPageURL;
	}

	public String getLoginURL() {
		return LoginURL;
	}

	public void setLoginURL(String loginURL) {
		LoginURL = loginURL;
	}
	
	public int getValidateCodeLenth() {
		return ValidateCodeLenth;
	}

	public void setValidateCodeLenth(int validateCodeLenth) {
		ValidateCodeLenth = validateCodeLenth;
	}

	public int getValidateCodeImageHight() {
		return ValidateCodeImageHight;
	}

	public void setValidateCodeImageHight(int validateCodeImageHight) {
		ValidateCodeImageHight = validateCodeImageHight;
	}
	
	public int getValidateCodeImageTimeOut() {
		return ValidateCodeImageTimeOut;
	}

	public void setValidateCodeImageTimeOut(int validateCodeImageTimeOut) {
		ValidateCodeImageTimeOut = validateCodeImageTimeOut;
	}

	public int getValidateCodeImageWidth() {
		return ValidateCodeImageWidth;
	}

	public void setValidateCodeImageWidth(int validateCodeImageWidth) {
		ValidateCodeImageWidth = validateCodeImageWidth;
	}

	public int getRememberMeTime() {
		return RememberMeTime;
	}

	public void setRememberMeTime(int rememberMeTime) {
		RememberMeTime = rememberMeTime;
	}

	
}
