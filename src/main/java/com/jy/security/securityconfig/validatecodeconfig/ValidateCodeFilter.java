package com.jy.security.securityconfig.validatecodeconfig;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.web.HttpSessionSessionStrategy;
import org.springframework.social.connect.web.SessionStrategy;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.filter.OncePerRequestFilter;

import com.jy.security.config.handler.MyAuthenticationFailureHandler;
import com.jy.security.controller.ValidateImageController;
import com.jy.security.dto.ValidateImageEntity;
import com.jy.security.properties.JySecurityProperties;

//OncePerRequestFilter Sping提供的工具类，保证子类拦截器在每次请求，只被执行一次
public class ValidateCodeFilter extends OncePerRequestFilter {
	
	@Autowired
	private MyAuthenticationFailureHandler myAuthenticationFailureHandler;
	
	//Spring 工具类 用来操纵Session
	private SessionStrategy sessionStrategy = new HttpSessionSessionStrategy();
	@Autowired
	private JySecurityProperties jySecurityProperties;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		String  loginurl= jySecurityProperties.getLoginProperties().getLoginPageProperties().getLoginURL();
		//如果是登录请求，并且是post请求，才进行判断
		if(loginurl.equalsIgnoreCase(request.getRequestURI()) &&
			"post".equalsIgnoreCase(request.getMethod())){
			try{
				//验证码校验逻辑方法
				Validate(new ServletWebRequest(request));
			}
			catch (ValidateImageException e) {
				//如果捕获到异常，用身份认证失败处理器，来返回异常消息
				myAuthenticationFailureHandler.onAuthenticationFailure(request, response, e);
				//进入异常则直接跳出，不执行后面的过滤器
				return;
			}
			
		}
		
		filterChain.doFilter(request, response);
	}
	
	//校验逻辑
	private void Validate(ServletWebRequest servletWebRequest) throws ServletRequestBindingException{
		//获取验证码对象从session中
		ValidateImageEntity validateImageEntity = (ValidateImageEntity) sessionStrategy.getAttribute(servletWebRequest,
																					ValidateImageController.SESSION_KEY);
		//从表单中获取imagecode的值，验证输入栏的name
		String codeInRequest = ServletRequestUtils.getStringParameter(servletWebRequest.getRequest(), "imagecode");
		
		if(codeInRequest == null || "".equals(codeInRequest)){
			throw new ValidateImageException("验证码不能为空");
		}
		if(validateImageEntity == null ){
			throw new ValidateImageException("验证码不存在");
		}
		if(validateImageEntity.isExpried()){
			//验证码过期则移除此验证码
			sessionStrategy.removeAttribute(servletWebRequest, ValidateImageController.SESSION_KEY);
			throw new ValidateImageException("验证码已过期");
		}
		if(! codeInRequest.equals(validateImageEntity.getCode())){
			throw new ValidateImageException("验证码不正确");
		}
		//验证成功后从session中移除验证码
		sessionStrategy.removeAttribute(servletWebRequest, ValidateImageController.SESSION_KEY);
		
	}

	public MyAuthenticationFailureHandler getMyAuthenticationFailureHandler() {
		return myAuthenticationFailureHandler;
	}

	public void setMyAuthenticationFailureHandler(MyAuthenticationFailureHandler myAuthenticationFailureHandler) {
		this.myAuthenticationFailureHandler = myAuthenticationFailureHandler;
	}

	public SessionStrategy getSessionStrategy() {
		return sessionStrategy;
	}

	public void setSessionStrategy(SessionStrategy sessionStrategy) {
		this.sessionStrategy = sessionStrategy;
	}

	public JySecurityProperties getJySecurityProperties() {
		return jySecurityProperties;
	}

	public void setJySecurityProperties(JySecurityProperties jySecurityProperties) {
		this.jySecurityProperties = jySecurityProperties;
	}
	
	
}
