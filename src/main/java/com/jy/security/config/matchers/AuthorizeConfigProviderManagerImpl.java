package com.jy.security.config.matchers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

/*
 * 对所有实现AuthorizeConfigProvider接口的类，将其都配置到security，让其配置生效
 */
@Component("authorizeConfigProviderManager")
public class AuthorizeConfigProviderManagerImpl implements AuthorizeConfigProviderManager {
	
	@Autowired
	private List<AuthorizeConfigProvider> authorizeConfigProviders;
	
	@Override
	public void config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
		// TODO Auto-generated method stub
		
		for(AuthorizeConfigProvider authorizeConfigProvider:authorizeConfigProviders){
			authorizeConfigProvider.config(config);
		}
	/*	//除了循环的配置，其余URL都需要认证
		config.anyRequest().authenticated();*/

	}

}
