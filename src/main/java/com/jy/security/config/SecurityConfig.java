package com.jy.security.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import com.jy.security.config.handler.MyAuthenticationFailureHandler;
import com.jy.security.config.handler.MyAuthentivationSuccessHandler;
import com.jy.security.config.matchers.AuthorizeConfigProviderManager;
import com.jy.security.properties.JySecurityProperties;
import com.jy.security.securityconfig.sessionconfig.MerryyounExpiredSessionStrategy;
import com.jy.security.securityconfig.validatecodeconfig.ValidateCodeFilter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
	private JySecurityProperties jySecurityProperties;
	@Autowired
	private MyAuthentivationSuccessHandler myAuthentivationSuccessHandler;
	@Autowired
	private MyAuthenticationFailureHandler myAuthenticationFailureHandler;
	@Autowired
	private DataSource dataSource;
	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private AuthorizeConfigProviderManager authorizeConfigProviderManager;
	@Bean
	public PersistentTokenRepository tokenRepository(){
		JdbcTokenRepositoryImpl tokenRepository = new JdbcTokenRepositoryImpl();
		tokenRepository.setDataSource(dataSource);
		return tokenRepository;
	}
	@Bean
	public PasswordEncoder passwordEncoder(){
		
		return new BCryptPasswordEncoder();
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		ValidateCodeFilter validateCodeFilter = new ValidateCodeFilter();
		validateCodeFilter.setMyAuthenticationFailureHandler(myAuthenticationFailureHandler);
		validateCodeFilter.setJySecurityProperties(jySecurityProperties);
		http
		.addFilterBefore(validateCodeFilter, UsernamePasswordAuthenticationFilter.class)
		.formLogin()
				.loginPage(jySecurityProperties.getLoginProperties().getLoginPageProperties().getLoginPageURL())
				.loginProcessingUrl(jySecurityProperties.getLoginProperties().getLoginPageProperties().getLoginURL())
				.successHandler(myAuthentivationSuccessHandler)
				.failureHandler(myAuthenticationFailureHandler)
		.and()
			.rememberMe()
				.tokenRepository(tokenRepository())
				.tokenValiditySeconds(jySecurityProperties.getLoginProperties().getLoginPageProperties().getRememberMeTime())
				.userDetailsService(userDetailsService)
		.and()
			.sessionManagement()
				.invalidSessionUrl(jySecurityProperties.getSessionManagementProperties().getSessionTimeOutUrl())
				.maximumSessions(jySecurityProperties.getSessionManagementProperties().getMaximumSession())
				.maxSessionsPreventsLogin(jySecurityProperties.getSessionManagementProperties().isMaxSessionPreventsLogin())
				.expiredSessionStrategy(new MerryyounExpiredSessionStrategy())
		.and()
		.and()
			.logout()
				.logoutUrl(jySecurityProperties.getLoginProperties().getLogOutPageProperties().getLogoutUrl())
				.logoutSuccessUrl(jySecurityProperties.getLoginProperties().getLogOutPageProperties().getLogoutSuccessUrl())
				.deleteCookies(jySecurityProperties.getLoginProperties().getLogOutPageProperties().getDeleteCookies())
		.and()
			.csrf().disable();
		authorizeConfigProviderManager.config(http.authorizeRequests());
	}
		
}
