package com.jy.security.properties;

public class SessionManagementProperties {
	
	private int maximumSession = 1;
	
	private boolean maxSessionPreventsLogin = false;

	private String sessionTimeOutUrl = "/sessionTimeOut";
	
	public int getMaximumSession() {
		return maximumSession;
	}

	public void setMaximumSession(int maximumSession) {
		this.maximumSession = maximumSession;
	}

	public boolean isMaxSessionPreventsLogin() {
		return maxSessionPreventsLogin;
	}

	public void setMaxSessionPreventsLogin(boolean maxSessionPreventsLogin) {
		this.maxSessionPreventsLogin = maxSessionPreventsLogin;
	}

	public String getSessionTimeOutUrl() {
		return sessionTimeOutUrl;
	}

	public void setSessionTimeOutUrl(String sessionTimeOutUrl) {
		this.sessionTimeOutUrl = sessionTimeOutUrl;
	}
	
	
}
