package com.jy.security.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "joyou.security")
public class JySecurityProperties {
	
	private LoginProperties loginProperties = new LoginProperties();

	private SessionManagementProperties sessionManagementProperties = new SessionManagementProperties();
	
	public LoginProperties getLoginProperties() {
		return loginProperties;
	}

	public void setLoginProperties(LoginProperties loginProperties) {
		this.loginProperties = loginProperties;
	}

	public SessionManagementProperties getSessionManagementProperties() {
		return sessionManagementProperties;
	}

	public void setSessionManagementProperties(SessionManagementProperties sessionManagementProperties) {
		this.sessionManagementProperties = sessionManagementProperties;
	}
	
}
