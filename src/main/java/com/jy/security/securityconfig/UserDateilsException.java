/**
 * 
 */
package com.jy.security.securityconfig;

import org.springframework.security.core.AuthenticationException;

/**
 * @author Administrator
 *
 */
//AuthenticationException是security在身份认证过程中抛出异常的基类
public class UserDateilsException extends AuthenticationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5862438714145672381L;

	/**
	 * 
	 */
	
	public UserDateilsException(String msg) {
		super(msg);
	}

}
